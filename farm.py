# -*- coding: utf-8 -*-

from json import loads as json_loads, dumps as json_to_string
from os import makedirs
from os.path import exists as path_exists

from datamanager import DataManager, WrongPinException, HitAuthException
from checkout import Checkout
from dialogs import OkDialog


def date_cmp(date):
    return -int(date[6:] + date[3:5] + date[:2])


class Farm:
    @classmethod
    def from_string(cls, text):
        object_map = json_loads(text)
        return cls.from_object(object_map)

    @classmethod
    def from_object(cls, object_map):
        new_farm = cls(
            object_map["name"],
            object_map["address"],
            object_map["tel"],
            object_map["mail"],
            object_map["user"],
            object_map["password"],
        )
        new_farm.old_dates = object_map.get("oldDates", [])
        new_farm.new_dates = object_map.get("newDates", [])
        return new_farm

    def __init__(self, name, address, tel, mail, user, password):
        self.user = user
        self.data_manager = DataManager()
        self.password = password
        self.name = name
        self.address = address
        self.tel = tel
        self.mail = mail
        self.new_dates = []
        self.old_dates = []
        self.status = None
        self.status_observers = []
        self.date_observers = []
        self.path = "Berichte/" + self.name + "/"

    def release(self):
        self.status_observers = []
        self.date_observers = []
        self.data_manager.release()
        print('Farm "%s" released' % self.name)

    def register_status_observer(self, obs):
        if obs not in self.status_observers:
            self.status_observers.append(obs)

    def register_date_observer(self, obs):
        if obs not in self.date_observers:
            self.date_observers.append(obs)

    def unregister_status_observer(self, obs):
        if obs in self.status_observers:
            self.status_observers.remove(obs)

    def unregister_date_observer(self, obs):
        if obs in self.date_observers:
            self.date_observers.remove(obs)

    def notify_status_observers(self):
        for status_obs in self.status_observers:
            status_obs.status_notify(self.name, self.status)

    def notify_date_observers(self):
        for date_obs in self.date_observers:
            date_obs.date_notify(self.old_dates, self.new_dates)

    def set_dates(self, old_dates, new_dates):
        changed = False
        if new_dates != self.new_dates or old_dates != self.old_dates:
            changed = True

        self.new_dates = sorted(new_dates, key=date_cmp)
        self.old_dates = sorted(old_dates, key=date_cmp)

        if self.new_dates:
            self.set_status("new")
        else:
            self.set_status("old")

        if changed:
            self.notify_date_observers()

    def change_date(self, date, status):
        if date not in self.new_dates + self.old_dates:
            return

        old_dates = [i for i in self.old_dates]
        new_dates = [i for i in self.new_dates]

        if status == "old":
            if date in new_dates:
                new_dates.remove(date)
            if date not in old_dates:
                old_dates.append(date)
        elif status == "new":
            if date in old_dates:
                old_dates.remove(date)
            if date not in new_dates:
                new_dates.append(date)

        self.set_dates(old_dates, new_dates)

    def set_status(self, status):
        changed = False
        if status != self.status:
            changed = True

        self.status = status

        if changed:
            self.notify_status_observers()

    def to_string(self):
        return json_to_string(self.to_object())

    def to_object(self):
        object_map = {
            "name": self.name,
            "address": self.address,
            "tel": self.tel,
            "mail": self.mail,
            "user": self.user,
            "password": self.password,
            "oldDates": self.old_dates,
            "newDates": self.new_dates,
        }
        return object_map

    def apply_dates(self, dates):
        old_dates = list(self.old_dates)
        new_dates = list(self.new_dates)

        # if dates:
        #     for i in reversed(range(len(new_dates))):
        #         if new_dates[i] not in dates:
        #             del new_dates[i]

        #     for i in reversed(range(len(old_dates))):
        #         if old_dates[i] not in dates:
        #             del old_dates[i]

        for date in dates:
            if date not in new_dates and date not in old_dates:
                new_dates.append(date)

        self.set_dates(old_dates, new_dates)

    def mark_date(self, date, mark):
        if mark:
            if date not in self.old_dates:
                return
            self.old_dates.remove(date)
            self.new_dates.append(date)
        else:
            if date not in self.new_dates:
                return
            self.new_dates.remove(date)
            self.old_dates.append(date)

    def update(self):
        """ Updates status and date lists """
        try:
            self.data_manager.login(self.user, self.password)
        except WrongPinException:
            self.set_status("wrongPin")
            return
        except HitAuthException:
            self.set_status("hitAuthFailed")
            return

        dates = self.data_manager.get_probe_dates()
        self.data_manager.logout()
        self.apply_dates(dates)

    def get_report_data(self, date):
        try:
            self.data_manager.login(self.user, self.password)
        except WrongPinException:
            self.set_status("wrongPin")
            return "wrongPin", None
        except HitAuthException:
            self.set_status("hitAuthFailed")
            return "hitAuthFailed", None

        report_data = self.data_manager.get_report_data(date)
        self.data_manager.logout()

        if not report_data:
            return "nothingFound", None

        if not report_data["lastProbes"][0]:
            return "noLastData", None

        return None, report_data

    def make_report(self, date):
        if not date:
            return "noDate"
        if date not in self.new_dates + self.old_dates:
            return "wrongDate"

        filename = self.path + date + ".pdf"
        if path_exists(filename):
            return "fileExists"

        err, report_data = self.get_report_data(date)
        if err:
            if err == "noLastData":
                dlg = OkDialog(
                    None,
                    u"Keine Vergleichsmessung für Milchverlust gefunden!\n(Bericht wird trotzdem erstellt)",
                    title=u"Bericht erstellen",
                )
                dlg.ShowModal()
                dlg.Destroy()
            else:
                return err

        if not path_exists(self.path):
            makedirs(self.path)

        checkout = Checkout(report_data, filename)
        checkout.generate_report()

        return None
