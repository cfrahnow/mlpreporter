# -*- coding: utf-8 -*-

from os import makedirs
from os.path import exists as pathExists
import matplotlib.pyplot as plt


class Diagram:
    def __init__(self):
        self.path = "temp/"
        self.milk_dist_min_max = [0, 380, 0, 50]
        self.energy_supply_min_max = [0, 55, 2.0, 4.5]
        self.acidose_danger_min_max = [0, 55, 1.5, 6.5]
        self.borders = {"protein": [3.1, 3.9], "fat": [3.8, 4.8]}
        self.square_color = "#33DD33"
        self.circle_color = "#008800"

    def save_plot(self, plot, filename):
        if not pathExists(self.path):
            makedirs(self.path)

        plot.savefig(self.path + filename, bbox_inches="tight")

    @staticmethod
    def apply_borders(data, borders):
        for entry in data:
            entry = min(max(entry, borders[0]), borders[1])

    def make_milk_dist_diag(self, squared_data, dotted_data, filename):
        figure = plt.figure()
        subplot = figure.add_subplot(111)

        subplot.set_xlabel("Laktationstage")
        subplot.set_ylabel("Milchmenge")

        self.apply_borders(squared_data[0], self.milk_dist_min_max[:2])
        self.apply_borders(squared_data[1], self.milk_dist_min_max[2:])
        self.apply_borders(dotted_data[0], self.milk_dist_min_max[:2])
        self.apply_borders(dotted_data[1], self.milk_dist_min_max[2:])

        x_ticks = range(self.milk_dist_min_max[0], self.milk_dist_min_max[1], 50)
        y_ticks = [i for i in range(int(self.milk_dist_min_max[2]), int(self.milk_dist_min_max[3]), 5)]

        subplot.set_xticks(x_ticks)
        subplot.set_yticks(y_ticks)

        subplot.grid(which="major", alpha=0.5)

        subplot.plot(
            squared_data[0],
            squared_data[1],
            color=self.square_color,
            marker="s",
            linestyle="None",
            label="Erste Laktation",
        )
        subplot.plot(
            dotted_data[0],
            dotted_data[1],
            color=self.circle_color,
            marker="o",
            linestyle="None",
            label="Mehr als 1 Laktation",
        )

        subplot.legend(loc=1)

        subplot.axis(self.milk_dist_min_max)
        self.save_plot(figure, filename)
        plt.close(figure)

    def make_energy_supply_diag(self, squared_data, dotted_data, filename):
        figure = plt.figure()
        subplot = figure.add_subplot(111)

        subplot.set_xlabel(u"Milchmenge")
        subplot.set_ylabel(u"Eiweißgehalte")

        self.apply_borders(squared_data[0], self.energy_supply_min_max[:2])
        self.apply_borders(squared_data[1], self.energy_supply_min_max[2:])
        self.apply_borders(dotted_data[0], self.energy_supply_min_max[:2])
        self.apply_borders(dotted_data[1], self.energy_supply_min_max[2:])

        x_ticks = range(self.energy_supply_min_max[0], self.energy_supply_min_max[1], 10)
        y_ticks = [
            i / 10.0
            for i in range(
                int(self.energy_supply_min_max[2] * 10),
                int(self.energy_supply_min_max[3] * 10),
                2,
            )
        ]

        subplot.set_xticks(x_ticks)
        subplot.set_yticks(y_ticks)
        subplot.grid(which="major", alpha=0.5)

        subplot.axhline(
            y=self.borders["protein"][0],
            xmin=self.energy_supply_min_max[0] - 1,
            xmax=self.energy_supply_min_max[0] + 1,
            linewidth=2,
            color="#550000",
        )
        subplot.axhline(
            y=self.borders["protein"][1],
            xmin=self.energy_supply_min_max[0] - 1,
            xmax=self.energy_supply_min_max[0] + 1,
            linewidth=2,
            color="#550000",
        )
        subplot.plot(
            squared_data[0],
            squared_data[1],
            color=self.square_color,
            marker="s",
            linestyle="None",
            label="Erste Laktation",
        )
        subplot.plot(
            dotted_data[0],
            dotted_data[1],
            color=self.circle_color,
            marker="o",
            linestyle="None",
            label="Mehr als 1 Laktation",
        )

        subplot.legend(loc=1)

        subplot.axis(self.energy_supply_min_max)
        self.save_plot(figure, filename)
        plt.close(figure)

    def make_acidose_danger_diag(self, squared_data, dotted_data, filename):
        figure = plt.figure()
        subplot = figure.add_subplot(111)

        subplot.set_xlabel(u"Milchmenge")
        subplot.set_ylabel(u"Fettgehalt")

        self.apply_borders(squared_data[0], self.acidose_danger_min_max[:2])
        self.apply_borders(squared_data[1], self.acidose_danger_min_max[2:])
        self.apply_borders(dotted_data[0], self.acidose_danger_min_max[:2])
        self.apply_borders(dotted_data[1], self.acidose_danger_min_max[2:])

        x_ticks = range(self.acidose_danger_min_max[0], self.acidose_danger_min_max[1], 10)
        y_ticks = [
            i / 10.0
            for i in range(
                int(self.acidose_danger_min_max[2] * 10),
                int(self.acidose_danger_min_max[3] * 10),
                5,
            )
        ]

        subplot.set_xticks(x_ticks)
        subplot.set_yticks(y_ticks)

        subplot.grid(which="major", alpha=0.5)

        subplot.axhline(
            y=self.borders["fat"][0],
            xmin=self.acidose_danger_min_max[0] - 1,
            xmax=self.acidose_danger_min_max[0] + 1,
            linewidth=2,
            color="#550000",
        )
        subplot.axhline(
            y=self.borders["fat"][1],
            xmin=self.acidose_danger_min_max[0] - 1,
            xmax=self.acidose_danger_min_max[0] + 1,
            linewidth=2,
            color="#550000",
        )
        subplot.plot(
            squared_data[0],
            squared_data[1],
            color=self.square_color,
            marker="s",
            linestyle="None",
            label="Erste Laktation",
        )
        subplot.plot(
            dotted_data[0],
            dotted_data[1],
            color=self.circle_color,
            marker="o",
            linestyle="None",
            label="Mehr als 1 Laktation",
        )

        subplot.legend(loc=1)

        subplot.axis(self.acidose_danger_min_max)
        self.save_plot(figure, filename)
        plt.close(figure)


if __name__ == "__main__":
    d = Diagram()
    d.make_milk_dist_diag(([-1, 2, 3, 500], [-1, 4, 9, 100]), ([1, 2, 3, 4], [2, 3, 4, 5]), "test.png")
