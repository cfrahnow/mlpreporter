# -*- coding: utf-8 -*-

import sys
from os import rename
from os.path import exists as path_exists
from typing import Optional
from wx import ID_OK

from dialogs import NewFarmDialog, OkDialog
from farm import Farm


class FarmManager:
    def __init__(self):
        self.farms = {}
        self.farm_observers = []
        self.active_farm: Optional[Farm] = None

        self.new_farms = []
        self.old_farms = []
        self.wrong_pin_farms = []

        self.load_farms()

    def release(self):
        self.active_farm = None
        for farm_name in self.farms:
            self.farms[farm_name].release()

        self.farm_observers = []
        print("FM released")
        sys.stdout.flush()

    def set_farms(self, old_farms, new_farms, wrong_pin_farms):
        old_farms = sorted(old_farms)
        new_farms = sorted(new_farms)
        wrong_pin_farms = sorted(wrong_pin_farms)

        changed = False
        if old_farms != self.old_farms or new_farms != self.new_farms or wrong_pin_farms != self.wrong_pin_farms:
            changed = True

        self.old_farms = old_farms
        self.new_farms = new_farms
        self.wrong_pin_farms = wrong_pin_farms

        if changed:
            self.notify_farm_observers()
            self.save_farms()

    def load_farms(self):
        self.farms = {}
        if path_exists("farms.txt"):
            with open("farms.txt", "r") as farm_file:
                for line in farm_file:
                    new_farm = Farm.from_string(line.strip())
                    new_farm.register_status_observer(self)
                    new_farm.register_date_observer(self)
                    self.farms[new_farm.name] = new_farm

    def add_farm(self):
        dlg = NewFarmDialog(None, None, title="Neuer Betrieb")
        result = dlg.ShowModal()
        dlg.Destroy()
        if result != ID_OK:
            return

        if not (dlg.name and dlg.user and dlg.password):
            return "notComplete"
        elif dlg.name in self.farms:
            return "nameExists"
        elif path_exists("Berichte/" + dlg.name):
            return "dirExists"

        new_farm = Farm(dlg.name, dlg.address, dlg.tel, dlg.mail, dlg.user, dlg.password)
        new_farm.register_status_observer(self)
        new_farm.register_date_observer(self)
        self.farms[dlg.name] = new_farm
        new_farm.update()
        return None

    def change_farm(self):
        if not self.active_farm:
            return "noSelection"
        old_farm = self.active_farm

        dlg = NewFarmDialog(None, old_farm.to_object(), title="Neuer Betrieb")
        result = dlg.ShowModal()
        dlg.Destroy()
        if result != ID_OK:
            return

        if not (dlg.name and dlg.user and dlg.password):
            return "notComplete"
        elif dlg.name != old_farm.name and dlg.name in self.farms:
            return "nameExists"
        elif dlg.name.upper() != old_farm.name.upper() and path_exists("Berichte/" + dlg.name):
            return "dirExists"

        old_farm.release()
        del self.farms[old_farm.name]
        new_farm = Farm(dlg.name, dlg.address, dlg.tel, dlg.mail, dlg.user, dlg.password)
        new_farm.register_status_observer(self)
        new_farm.register_date_observer(self)
        self.farms[dlg.name] = new_farm
        self.active_farm = new_farm
        self.active_farm.old_dates = old_farm.old_dates
        self.active_farm.new_dates = old_farm.new_dates

        if path_exists(old_farm.path):
            rename(old_farm.path, self.active_farm.path)

        self.active_farm.update()

    def remove_farm(self):
        if not self.active_farm:
            return "noSelection"

        dlg = OkDialog(None, u"%s wirklich löschen?" % self.active_farm.name, title=u"Löschen")
        result = dlg.ShowModal()
        dlg.Destroy()

        if result == ID_OK:
            old_farms = [i for i in self.old_farms]
            new_farms = [i for i in self.new_farms]
            wrong_pin_farms = [i for i in self.wrong_pin_farms]
            name = self.active_farm.name
            self.active_farm.release()
            del self.farms[name]
            self.activate_farm(None)
            if name in old_farms:
                old_farms.remove(name)
            if name in new_farms:
                new_farms.remove(name)
            if name in wrong_pin_farms:
                wrong_pin_farms.remove(name)
            self.set_farms(old_farms, new_farms, wrong_pin_farms)

    def update_farms(self):
        for farm_name in self.farms:
            self.farms[farm_name].update()

    def activate_farm(self, farm_name):
        if farm_name is None:
            self.active_farm = farm_name
            return

        if farm_name not in self.farms:
            print("Farm %s not in farms of FarmManager" % farm_name)
            return

        self.active_farm = self.farms[farm_name]
        self.active_farm.notify_date_observers()

    def save_farms(self):
        with open("farms.txt", "w") as farm_file:
            for farm in self.farms.values():
                farm_file.write(farm.to_string() + "\n")

    def change_date(self, date, status):
        self.active_farm.change_date(date, status)

    def register_farm_observer(self, observer):
        if observer not in self.farm_observers:
            self.farm_observers.append(observer)

    def unregister_farm_observer(self, observer):
        if observer in self.farm_observers:
            self.farm_observers.remove(observer)

    def notify_farm_observers(self):
        for farm_observer in self.farm_observers:
            farm_observer.farm_notify()

    def register_date_observer(self, farm_name, observer):
        if farm_name is None:
            observer.date_notify([], [])
            return

        if farm_name not in self.farms:
            print("Farm %s not in farms of FarmManager" % farm_name)
            return

        self.farms[farm_name].register_date_observer(observer)
        self.activate_farm(farm_name)

    def unregister_date_observer(self, farm_name, observer):
        if farm_name not in self.farms:
            print("Farm %s not in farms of FarmManager" % farm_name)
            return

        self.farms[farm_name].unregister_date_observer(observer)
        self.activate_farm(None)

    def status_notify(self, farm_name, status):
        old_farms = list(self.old_farms)
        new_farms = list(self.new_farms)
        wrong_pin_farms = list(self.wrong_pin_farms)

        if status == "old":
            if farm_name in new_farms:
                new_farms.remove(farm_name)
            if farm_name in wrong_pin_farms:
                wrong_pin_farms.remove(farm_name)
            if farm_name not in old_farms:
                old_farms.append(farm_name)
        elif status == "new":
            if farm_name in old_farms:
                old_farms.remove(farm_name)
            if farm_name in wrong_pin_farms:
                wrong_pin_farms.remove(farm_name)
            if farm_name not in new_farms:
                new_farms.append(farm_name)
        elif status in ["wrongPin", "hitAuthFailed"]:
            if farm_name in old_farms:
                old_farms.remove(farm_name)
            if farm_name in new_farms:
                new_farms.remove(farm_name)
            if farm_name not in wrong_pin_farms:
                wrong_pin_farms.append(farm_name)

        self.set_farms(old_farms, new_farms, wrong_pin_farms)

    def date_notify(self, *_):
        self.save_farms()

    def make_report(self, date):
        return self.active_farm.make_report(date)
