# -*- coding: utf-8 -*-

from wx.lib import sized_controls
from wx import (
    ID_ANY,
    TextCtrl,
    StaticText,
    StaticLine,
    LI_HORIZONTAL,
    ID_OK,
    Button,
    EVT_BUTTON,
    ID_CANCEL,
)

BORDER_MAP_VALUES = {
    u"Lakt. Zahl": [-1000000.0, 1000000.0],
    u"Harnstoff- Klasse": [-1000000.0, 1000000.0],
    u"ZZ": [-1000000.0, 101.0],
    u"Milch kg": [20.0, 26.0],
    u"Lakt. Tage": [-1000000.0, 1000000.0],
    u"FEQ": [0.9, 1.5],
    u"Fett %": [3.7, 4.0],
    u"Eiw %": [3.1, 3.9],
    u"Harnstoff": [14.0, 31.0],
}


class BorderDialog(sized_controls.SizedDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.old_border_map = BORDER_MAP_VALUES
        self.border_map = None
        pane = self.GetContentsPane()

        input_pane = sized_controls.SizedPanel(pane)
        input_pane.SetSizerType("vertical")
        input_pane.SetSizerProps(align="center")

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.lakzahl_min = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Lakt. Zahl"][0]).replace(".", ","),
        )
        StaticText(line_pane, id=ID_ANY, label=u"< Laktations Zahl <")
        self.lakzahl_max = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Lakt. Zahl"][1]).replace(".", ","),
        )

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.laktage_min = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Lakt. Tage"][0]).replace(".", ","),
        )
        StaticText(line_pane, id=ID_ANY, label=u"< Laktations Tage <")
        self.laktage_max = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Lakt. Tage"][1]).replace(".", ","),
        )

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.milch_min = TextCtrl(line_pane, value=str(self.old_border_map[u"Milch kg"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< Milch kg <")
        self.milch_max = TextCtrl(line_pane, value=str(self.old_border_map[u"Milch kg"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.fett_min = TextCtrl(line_pane, value=str(self.old_border_map[u"Fett %"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< Fett % <")
        self.fett_max = TextCtrl(line_pane, value=str(self.old_border_map[u"Fett %"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.eiw_min = TextCtrl(line_pane, value=str(self.old_border_map[u"Eiw %"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< Eiweiß % <")
        self.eiw_max = TextCtrl(line_pane, value=str(self.old_border_map[u"Eiw %"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.zz_min = TextCtrl(line_pane, value=str(self.old_border_map[u"ZZ"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< Zellzahl <")
        self.zz_max = TextCtrl(line_pane, value=str(self.old_border_map[u"ZZ"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.feq_min = TextCtrl(line_pane, value=str(self.old_border_map[u"FEQ"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< FEQ <")
        self.feq_max = TextCtrl(line_pane, value=str(self.old_border_map[u"FEQ"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.h_min = TextCtrl(line_pane, value=str(self.old_border_map[u"Harnstoff"][0]).replace(".", ","))
        StaticText(line_pane, id=ID_ANY, label=u"< Harnstoff <")
        self.h_max = TextCtrl(line_pane, value=str(self.old_border_map[u"Harnstoff"][1]).replace(".", ","))

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(align="center")
        self.hkl_min = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Harnstoff- Klasse"][0]).replace(".", ","),
        )
        StaticText(line_pane, id=ID_ANY, label=u"< Harnstoff-Klasse <")
        self.hkl_max = TextCtrl(
            line_pane,
            value=str(self.old_border_map[u"Harnstoff- Klasse"][1]).replace(".", ","),
        )

        static_line = StaticLine(pane, style=LI_HORIZONTAL)
        static_line.SetSizerProps(border=(("all", 0)), expand=True)

        pane_btns = sized_controls.SizedPanel(pane)
        pane_btns.SetSizerType("horizontal")
        pane_btns.SetSizerProps(align="center")

        button_ok = Button(pane_btns, ID_OK, label="Speichern")
        button_ok.Bind(EVT_BUTTON, self.on_ok)

        button_cancel = Button(pane_btns, ID_CANCEL, label="Abbrechen")
        button_cancel.Bind(EVT_BUTTON, self.on_cancel)

        self.Fit()

    def on_cancel(self, event):
        dlg = OkDialog(None, u"Mögliche Änderungen verwerfen?", title="Grenzen bearbeiten")
        result = dlg.ShowModal()
        dlg.Destroy()
        if result != ID_OK:
            return

        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()

    def on_ok(self, event):
        try:
            lakzahl_min = float(self.lakzahl_min.GetValue().replace(",", "."))
            lakzahl_max = float(self.lakzahl_max.GetValue().replace(",", "."))
            laktage_min = float(self.laktage_min.GetValue().replace(",", "."))
            laktage_max = float(self.laktage_max.GetValue().replace(",", "."))
            milch_min = float(self.milch_min.GetValue().replace(",", "."))
            milch_max = float(self.milch_max.GetValue().replace(",", "."))
            fett_min = float(self.fett_min.GetValue().replace(",", "."))
            fett_max = float(self.fett_max.GetValue().replace(",", "."))
            eiw_min = float(self.eiw_min.GetValue().replace(",", "."))
            eiw_max = float(self.eiw_max.GetValue().replace(",", "."))
            zz_min = float(self.zz_min.GetValue().replace(",", "."))
            zz_max = float(self.zz_max.GetValue().replace(",", "."))
            feq_min = float(self.feq_min.GetValue().replace(",", "."))
            feq_max = float(self.feq_max.GetValue().replace(",", "."))
            h_min = float(self.h_min.GetValue().replace(",", "."))
            h_max = float(self.h_max.GetValue().replace(",", "."))
            hkl_min = float(self.hkl_min.GetValue().replace(",", "."))
            hkl_max = float(self.hkl_max.GetValue().replace(",", "."))
        except ValueError:
            dlg = OkDialog(None, u"Alle Werte müssen Zahlen sein!", title=u"Ungültige Eingabe")
            dlg.ShowModal()
            dlg.Destroy()
            return

        self.border_map = {
            u"Lakt. Zahl": [lakzahl_min, lakzahl_max],
            u"Lakt. Tage": [laktage_min, laktage_max],
            u"Milch kg": [milch_min, milch_max],
            u"Fett %": [fett_min, fett_max],
            u"Eiw %": [eiw_min, eiw_max],
            u"ZZ": [zz_min, zz_max],
            u"FEQ": [feq_min, feq_max],
            u"Harnstoff": [h_min, h_max],
            u"Harnstoff- Klasse": [hkl_min, hkl_max],
        }

        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()


class NewFarmDialog(sized_controls.SizedDialog):
    def __init__(self, *args, **kwargs):
        super(NewFarmDialog, self).__init__(*args[:-1], **kwargs)
        self.name = None
        self.address = None
        self.tel = None
        self.mail = None
        self.user = None
        self.password = None

        if args[-1]:
            values = args[-1]
        else:
            values = {
                "name": "",
                "address": "",
                "tel": "",
                "mail": "",
                "user": "",
                "password": "",
            }

        pane = self.GetContentsPane()

        input_pane = sized_controls.SizedPanel(pane)
        input_pane.SetSizerType("vertical")
        input_pane.SetSizerProps(align="center")

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="Betrieb-Name")
        self.name_input = TextCtrl(line_pane, value=values["name"])

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="Adresse")
        self.address_input = TextCtrl(line_pane, value=values["address"])

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="Telefon")
        self.tel_input = TextCtrl(line_pane, value=values["tel"])

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="E-Mail")
        self.mail_input = TextCtrl(line_pane, value=values["mail"])

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="MLP Nummer")
        self.user_input = TextCtrl(line_pane, value=values["user"])

        line_pane = sized_controls.SizedPanel(input_pane)
        line_pane.SetSizerType("horizontal")
        line_pane.SetSizerProps(halign="right")
        StaticText(line_pane, id=ID_ANY, label="MLP PIN")
        self.password_input = TextCtrl(line_pane, value=values["password"])

        static_line = StaticLine(pane, style=LI_HORIZONTAL)
        static_line.SetSizerProps(border=(("all", 0)), expand=True)

        pane_btns = sized_controls.SizedPanel(pane)
        pane_btns.SetSizerType("horizontal")
        pane_btns.SetSizerProps(align="center")

        button_ok = Button(pane_btns, ID_OK, label="Fertig")
        button_ok.Bind(EVT_BUTTON, self.on_button)

        button_cancel = Button(pane_btns, ID_CANCEL, label="Abbrechen")
        button_cancel.Bind(EVT_BUTTON, self.on_button)

        self.Fit()

    def on_button(self, event):
        self.name = self.name_input.GetValue()
        self.address = self.address_input.GetValue()
        self.tel = self.tel_input.GetValue()
        self.mail = self.mail_input.GetValue()
        self.user = self.user_input.GetValue()
        self.password = self.password_input.GetValue()

        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()


class OkDialog(sized_controls.SizedDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args[:-1], **kwargs)
        text = args[-1]
        pane = self.GetContentsPane()

        StaticText(pane, id=ID_ANY, label=text)

        pane_btns = sized_controls.SizedPanel(pane)
        pane_btns.SetSizerType("horizontal")
        pane_btns.SetSizerProps(align="center")

        button_ok = Button(pane_btns, ID_OK, label="Ok")
        button_ok.Bind(EVT_BUTTON, self.on_button)

        button_cancel = Button(pane_btns, ID_CANCEL, label="Abbrechen")
        button_cancel.Bind(EVT_BUTTON, self.on_button)

        self.Fit()

    def on_button(self, event):
        if self.IsModal():
            self.EndModal(event.EventObject.Id)
        else:
            self.Close()
