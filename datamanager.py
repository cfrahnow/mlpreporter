from re import findall
from time import time as now, strftime, localtime

from bs4 import BeautifulSoup
from bs4.element import Tag as ElementTag
from requests import Session


class WrongPinException(Exception):
    pass


class HitAuthException(Exception):
    pass


LOGIN_URL = "https://gate.lkvbw.de/Portal/faces/Login.jsp"
NO_LOGOUT_URL = "https://gate.lkvbw.de/Portal/faces/noLogoutNote.jsp"
BUTTON_URL = "https://gate.lkvbw.de/Portal/faces/showMsgs.jsp"
CHOOSE_APP_URL = "https://gate.lkvbw.de/Portal/faces/chooseApp.jsp"
TIERLISTE_URL = "https://gate.lkvbw.de/RDVNG/Betrieb/Tierliste.jsf"
PROBE_URL = "https://gate.lkvbw.de/RDVNG/Betrieb/Probemelkungen.jsf"
PROBE_DETAIL_URL = "https://gate.lkvbw.de/RDVNG/Betrieb/ProbemelkungDetail.jsf"
LOGOUT_URL = "https://gate.lkvbw.de/RDVNG/logout.jsf"


def date_cmp(date):
    return -int(date[6:] + date[3:5] + date[:2])


class DataManager:
    def __init__(self):
        self.years = {}
        self.viewstate = None
        self.session = None

    def release(self):
        self.logout()

    def forgot_logout(self, text):
        return len(findall("<title>Lkv BW Portal Hinweis</title>", text)) > 0

    def get_signature_data(self, text):
        signature_match = findall(
            r"focRdv4mWin\(\'gate\.lkvbw\.de\', \'(.*?)\', \'(.*?)\', \'false\', \'(.*?)\'",
            text,
        )

        if signature_match:
            return signature_match[0]
        return None

    def login(self, username, password):
        self.session = Session()
        response = self.session.get(LOGIN_URL)
        values = {
            "_id0:userName": username,
            "_id0:userPwd": password,
            "_id0": "_id0",
            "_id0:_id11": "Anmelden",
        }
        response = self.session.post(LOGIN_URL, data=values)

        has_forgot_logout = self.forgot_logout(response.text)
        if has_forgot_logout:
            print("Forgot to logout last time!")
            values = {"_id13:_id14": "Weiter ->", "_id13": "_id13"}
            response = self.session.post(NO_LOGOUT_URL, data=values)

        if "errorMsgs" in response.text:
            raise HitAuthException()

        self.goto_herden_manager()

    def logout(self):
        if not self.session:
            return
        values = {
            "topMenuForm": "topMenuForm",
            "javax.faces.ViewState": self.get_viewstate(),
            "j_idt51": "j_idt51",
        }
        self.session.post(PROBE_URL, params=values)
        self.session.get(LOGOUT_URL)
        values = {"_id2:_id4": "Logout", "_id2": "_id2"}
        self.session.post(BUTTON_URL, data=values)
        self.session.post(BUTTON_URL, data=values)
        self.viewstate = None
        self.session = None

    def goto_herden_manager(self):
        response = self.session.get(CHOOSE_APP_URL)
        soup = BeautifulSoup(response.text, "html.parser")
        forms = soup.find_all("form")
        form = None
        for form in forms:
            button = form.find(value="LKV Herdenmanager")
            if button:
                break
        if not button:
            raise Exception('Button with value "LKV Herdenmanager" not found!')

        values = {form["id"]: form["id"], button["name"]: "LKV Herdenmanager"}

        response = self.session.post(CHOOSE_APP_URL, data=values)

        signature_data = self.get_signature_data(response.text)
        if not signature_data:
            raise WrongPinException()
        userid, betriebsnr, signature = signature_data

        self.session.get(
            "https://gate.lkvbw.de/RDVNG/?userid=%s&betriebsnr=%s&usertype=%s&sig=%s&modul=rdv4m"
            % (userid, betriebsnr, "false", signature)
        )
        self.session.get(PROBE_URL)

    def get_viewstate(self):
        if not self.viewstate:
            response = self.session.get(PROBE_URL)
            self.set_viewstate(response.text)
        return self.viewstate

    def set_viewstate(self, html):
        if not html:
            self.viewstate = None

        viewstate_matches = findall(r'id\="j_id1\:javax\.faces\.ViewState\:0" value="(.*?)"', html)
        if viewstate_matches:
            self.viewstate = viewstate_matches[0]
        else:
            self.viewstate = None

    def make_year_request(self, year):
        self.viewstate = None
        values = {
            "javax.faces.partial.ajax": "true",
            "javax.faces.source": "tf:j_idt143",
            "javax.faces.partial.execute": "tf:j_idt143",
            "javax.faces.partial.render": "tf:jahrInput",
            "javax.faces.behavior.event": "valueChange",
            "javax.faces.partial.event": "change",
            "tf": "tf",
            "javax.faces.ViewState": self.get_viewstate(),
            "tf:j_idt143": "true",
        }
        self.session.post(PROBE_URL, params=values)

        values = {
            "tf": "tf",
            "tf:j_idt143": "true",
            "tf:jahrInput": year,
            "tf:j_idt149": "",
            "javax.faces.ViewState": self.get_viewstate(),
        }
        response = self.session.post(PROBE_URL, params=values)

        return response.text

    def make_probe_date_request(self, year, button_name):
        self.make_year_request(year)
        values = {
            "javax.faces.partial.ajax": "true",
            "javax.faces.source": button_name,
            "javax.faces.partial.execute": "@all",
            button_name: button_name,
            "tf": "tf",
            "tf:j_idt143": "false",
            "javax.faces.ViewState": self.get_viewstate(),
        }
        self.session.post(PROBE_URL, params=values)
        response = self.session.get(PROBE_DETAIL_URL)
        self.set_viewstate(response.text)

        values = {
            "javax.faces.partial.ajax": "true",
            "javax.faces.source": "tf:pmDetailTable",
            "javax.faces.partial.execute": "tf:pmDetailTable",
            "javax.faces.partial.render": "tf:pmDetailTable",
            "tf:pmDetailTable": "tf:pmDetailTable",
            "tf:pmDetailTable_pagination": "true",
            "tf:pmDetailTable_first": 0,
            "tf:pmDetailTable_rows": 9999,
            "tf:pmDetailTable_skipChildren": "true",
            "tf:pmDetailTable_encodeFeature": "true",
            "tf": "tf",
            "tf:pmFilter_active": 1,
            "tf:pmDetailTable_rppDD": 9999,
            "javax.faces.ViewState": self.get_viewstate(),
        }
        self.session.post(PROBE_DETAIL_URL, params=values)
        response = self.session.get(PROBE_DETAIL_URL)

        return response.text

    def get_probes_for_year(self, year):
        if not year in self.years:
            html = self.make_year_request(year)
            data = self.convert_summary_data(html)
            self.years[year] = data
        head, body = self.years[year]
        new_body = {}
        for body_entries in body:
            new_body[body_entries] = list(body[body_entries])
        return list(head), new_body

    def get_probe_dates(self):
        next_year = int(strftime("%Y", localtime(now()))) + 1
        dates = []
        year = next_year

        while len(dates) < 12 and year > next_year - 3:
            dates += self.get_probes_for_year(year)[1].keys()
            year -= 1
        return dates

    def get_summary_data(self, date):
        years_to_search = [int(date[-4:]) - 1, int(date[-4:]), int(date[-4:]) + 1]
        data = {}
        head = []
        for year in years_to_search:
            head, new_data = self.get_probes_for_year(year)
            data.update(new_data)

        if date not in data:
            return None

        date_list = sorted(data.keys(), key=date_cmp)
        date_index = date_list.index(date)

        result_data = [data[date] for date in date_list[date_index : date_index + 4]]

        return [head, result_data]

    def get_probe_data(self, date):
        years_to_search = [int(date[-4:]), int(date[-4:]) + 1]
        button_name = None
        year_of_date = None
        for year in years_to_search:
            head, dates = self.get_probes_for_year(year)
            btn_index = head.index("ButtonName")
            if date in dates:
                button_name = dates[date][btn_index]
                year_of_date = year
                break

        if not button_name:
            return None

        html = self.make_probe_date_request(year_of_date, button_name)
        return self.convert_probe_date_data(html)

    def get_report_data(self, date):
        summary_data = self.get_summary_data(date)
        if summary_data is None:
            print("Datum %s nicht gefunden" % str(date))
            return None
        dates = sorted([entry[0] for entry in summary_data[1]], key=date_cmp)

        probe_data = self.get_probe_data(date)
        last_probe_data = self.get_probe_data(dates[1]) if len(dates) > 1 else [[], []]

        if not probe_data or not summary_data:
            return None

        return {
            "date": date,
            "probes": probe_data,
            "lastProbes": last_probe_data,
            "summary": summary_data,
        }

    def convert_summary_data(self, text):
        soup = BeautifulSoup(text, "html.parser")
        head = soup.find_all(id="tf:probemelkungen_head")[0]
        body = soup.find_all(id="tf:probemelkungen_data")[0]

        head_titles = []
        body_entries = {}

        headline = head.find_all(attrs={"class": "ui-column-title"})
        for entry in headline:
            result = ""
            for col_name in entry.span.children:
                result += " " if isinstance(col_name, ElementTag) else col_name
            head_titles.append(result)

        head_titles.append(u"ButtonName")

        date_index = head_titles.index("Probedatum")

        lines = body.find_all("tr")
        for line in lines:
            cols = line.find_all("td")
            entries = []
            if len(cols) < 2:
                return [head_titles, body_entries]
            for col in cols:
                entries.append(col.string)
                button_name = cols[date_index].find("a").get("id")
            entries.append(button_name)
            date = entries[date_index]
            body_entries[date] = entries

        return [head_titles, body_entries]

    def convert_probe_date_data(self, text):
        soup = BeautifulSoup(text, "html.parser")
        head = soup.find_all(id="tf:pmDetailTable_head")[0]
        body = soup.find_all(id="tf:pmDetailTable_data")[0]

        head_titles = []
        body_entries = []

        headline = head.find_all(attrs={"class": "ui-column-title"})
        for entry in headline:
            result = ""
            for col_name in entry.span.children:
                result += " " if isinstance(col_name, ElementTag) else col_name
            head_titles.append(result.strip())

        lines = body.find_all("tr")
        for line in lines:
            cols = line.find_all("td")
            entries = []
            for col in cols:
                entries.append(col.string)
            body_entries.append(entries)

        return [head_titles, body_entries]
