# -*- coding: utf-8 -*-

from wx import (  # pylint: disable=no-name-in-module
    Frame,
    BoxSizer,
    VERTICAL,
    HORIZONTAL,
    EXPAND,
    ALL,
    ListBox,
    ID_ANY,
    ID_OK,
    Button,
    EVT_BUTTON,
    StaticText,
    Font,
    CENTER,
    LB_NEEDED_SB,
    LB_HSCROLL,
    EVT_LISTBOX,
    NORMAL,
    ITALIC,
    DECORATIVE,
)

from requests.exceptions import ConnectionError as RequestConnectionError

from farm_manager import FarmManager
from dialogs import OkDialog, BorderDialog

# These should be imported from wx, but linter would complain
NORMAL = 90
ITALIC = 93
DECORATIVE = 71


class Window(Frame):
    def __init__(self):
        super(Window, self).__init__(None, title="Datenprogramm", size=(800, 600))
        self.lb_new_farms = None
        self.lb_old_farms = None
        self.lb_wrong_pin_farms = None
        self.btn_new_farm = None
        self.btn_change_farm = None
        self.btn_del_farm = None
        self.lb_new_dates = None
        self.lb_old_dates = None
        self.btn_mark_as_done = None
        self.btn_mark_as_not_done = None
        self.btn_checkout = None
        self.btn_borders = None
        try:
            self.farm_manager = FarmManager()
            connection_error = False
        except Exception as err:  # pylint: disable=broad-except
            print(err)
            self.farm_manager = None
            connection_error = True
        self.selected_date = None
        self.selected_farm = None
        self.init_ui()
        self.set_bindings()
        self.Centre()
        self.Show()
        if connection_error:
            dlg = OkDialog(
                None,
                "LKV Seite kann nicht erreicht werden (Internet aus?)",
                title="Fehler",
            )
            dlg.ShowModal()
            dlg.Destroy()
            exit(0)
            return

        self.farm_manager.register_farm_observer(self)
        try:
            self.farm_manager.update_farms()
        except RequestConnectionError:
            dlg = OkDialog(
                None,
                "LKV Seite kann nicht erreicht werden (Internet aus?)",
                title="Fehler",
            )
            dlg.ShowModal()
            dlg.Destroy()

    def release(self):
        if self.farm_manager is not None:
            self.farm_manager.release()
        print("Window released")

    def update_farm_boxes(self):
        self.lb_old_farms.SetItems(self.farm_manager.old_farms)
        self.lb_new_farms.SetItems(self.farm_manager.new_farms)
        self.lb_wrong_pin_farms.SetItems(self.farm_manager.wrong_pin_farms)

    def update_date_boxes(self, old_dates, new_dates):
        self.lb_old_dates.SetItems(old_dates)
        self.lb_new_dates.SetItems(new_dates)

    def farm_notify(self):
        self.update_farm_boxes()

    def date_notify(self, old_dates, new_dates):
        self.update_date_boxes(old_dates, new_dates)

    def create_title(self):
        """Creating the title label for the page."""
        title = StaticText(self, ID_ANY, "Programm")
        title.SetFont(Font(18, DECORATIVE, ITALIC, NORMAL))
        return title

    def create_content(self):
        content_sizer = BoxSizer(HORIZONTAL)
        farm_sizer = BoxSizer(VERTICAL)
        date_sizer = BoxSizer(VERTICAL)
        button_sizer = BoxSizer(VERTICAL)

        # Farm Boxes
        new_farm_label = StaticText(self, ID_ANY, "Betriebe mit neuen Messungen")
        old_farm_label = StaticText(self, ID_ANY, "Betriebe ohne neue Messungen")
        wrong_pin_farm_label = StaticText(self, ID_ANY, "Betriebe mit falschem PIN")
        self.lb_new_farms = ListBox(parent=self, style=LB_NEEDED_SB | LB_HSCROLL, name="Betriebe")
        self.lb_old_farms = ListBox(parent=self, style=LB_NEEDED_SB | LB_HSCROLL, name="Betriebe")
        self.lb_wrong_pin_farms = ListBox(parent=self, style=LB_NEEDED_SB | LB_HSCROLL, name="Betriebe")
        self.btn_new_farm = Button(self, ID_ANY, "Neuen Betrieb erstellen!")
        self.btn_change_farm = Button(self, ID_ANY, "Betrieb beabeiten!")
        self.btn_del_farm = Button(self, ID_ANY, "Betrieb entfernen!")

        farm_sizer.Add(new_farm_label, proportion=0)
        farm_sizer.Add(self.lb_new_farms, proportion=1, flag=EXPAND, border=0, userData=None)
        farm_sizer.AddSpacer(10)
        farm_sizer.Add(old_farm_label, proportion=0)
        farm_sizer.Add(self.lb_old_farms, proportion=1, flag=EXPAND, border=0, userData=None)
        farm_sizer.AddSpacer(10)
        farm_sizer.Add(wrong_pin_farm_label, proportion=0)
        farm_sizer.Add(self.lb_wrong_pin_farms, proportion=1, flag=EXPAND, border=0, userData=None)
        farm_sizer.AddSpacer(10)
        farm_sizer.Add(self.btn_new_farm, proportion=0)
        farm_sizer.AddSpacer(10)
        farm_sizer.Add(self.btn_change_farm, proportion=0)
        farm_sizer.AddSpacer(10)
        farm_sizer.Add(self.btn_del_farm, proportion=0)

        # Date Boxes
        new_date_label = StaticText(self, ID_ANY, "Neue Messungen")
        old_date_label = StaticText(self, ID_ANY, "Ausgewertete Messungen")
        self.lb_new_dates = ListBox(parent=self, style=LB_NEEDED_SB | LB_HSCROLL, name="Betriebe")
        self.lb_old_dates = ListBox(parent=self, style=LB_NEEDED_SB | LB_HSCROLL, name="Betriebe")
        date_sizer.Add(new_date_label, proportion=0)
        date_sizer.Add(self.lb_new_dates, proportion=1, flag=EXPAND, border=0, userData=None)
        date_sizer.AddSpacer(10)
        date_sizer.Add(old_date_label, proportion=0)
        date_sizer.Add(self.lb_old_dates, proportion=1, flag=EXPAND, border=0, userData=None)

        # Buttons
        self.btn_mark_as_done = Button(self, ID_ANY, 'Als "ausgewertet" markieren')
        self.btn_mark_as_not_done = Button(self, ID_ANY, 'Als "nicht ausgewertet" markieren')
        self.btn_checkout = Button(self, ID_ANY, "Auswerten")
        self.btn_borders = Button(self, ID_ANY, "Grenzen beabeiten")
        self.btn_checkout.SetFont(Font(16, DECORATIVE, ITALIC, NORMAL))
        button_sizer.Add(self.btn_mark_as_done, proportion=0, border=0, userData=None)
        button_sizer.AddSpacer(10)
        button_sizer.Add(self.btn_mark_as_not_done, proportion=0, border=0, userData=None)
        button_sizer.AddSpacer(20)
        button_sizer.Add(self.btn_checkout, proportion=0, border=0, userData=None)
        button_sizer.AddSpacer(40)
        button_sizer.Add(self.btn_borders, proportion=0, border=0, userData=None)

        content_sizer.Add(farm_sizer, 1, EXPAND | ALL, 10)
        content_sizer.Add(date_sizer, 1, EXPAND | ALL, 10)
        content_sizer.Add(button_sizer, 1, EXPAND | ALL, 10)

        return content_sizer

    def init_ui(self):
        main_sizer = BoxSizer(VERTICAL)

        title = self.create_title()
        content = self.create_content()

        main_sizer.AddSpacer(10)
        main_sizer.Add(title, 0, CENTER)
        main_sizer.AddSpacer(10)
        main_sizer.Add(content, 1, EXPAND)

        self.SetSizer(main_sizer)

    def set_bindings(self):
        self.Bind(EVT_LISTBOX, self.on_lb_farm_click, self.lb_new_farms)
        self.Bind(EVT_LISTBOX, self.on_lb_farm_click, self.lb_old_farms)
        self.Bind(EVT_LISTBOX, self.on_lb_farm_click, self.lb_wrong_pin_farms)

        self.Bind(EVT_LISTBOX, self.on_lb_date_click, self.lb_new_dates)
        self.Bind(EVT_LISTBOX, self.on_lb_date_click, self.lb_old_dates)

        self.Bind(EVT_BUTTON, self.on_mark_done_click, self.btn_mark_as_done)
        self.Bind(EVT_BUTTON, self.on_mark_not_done_click, self.btn_mark_as_not_done)
        self.Bind(EVT_BUTTON, self.on_checkout_click, self.btn_checkout)
        self.Bind(EVT_BUTTON, self.on_borders_click, self.btn_borders)

        self.Bind(EVT_BUTTON, self.on_new_farm_click, self.btn_new_farm)
        self.Bind(EVT_BUTTON, self.on_change_farm_click, self.btn_change_farm)
        self.Bind(EVT_BUTTON, self.on_del_farm_click, self.btn_del_farm)

    def activate_farm(self, farm_name):
        if self.selected_farm:
            self.farm_manager.unregister_date_observer(self.selected_farm, self)
        self.selected_farm = farm_name
        self.selected_date = None
        self.lb_old_farms.Deselect(self.lb_old_farms.GetSelection())
        self.lb_new_farms.Deselect(self.lb_new_farms.GetSelection())
        self.lb_wrong_pin_farms.Deselect(self.lb_wrong_pin_farms.GetSelection())
        if farm_name:
            self.lb_old_farms.SetStringSelection(farm_name)
            self.lb_new_farms.SetStringSelection(farm_name)
            self.lb_wrong_pin_farms.SetStringSelection(farm_name)

        self.farm_manager.register_date_observer(self.selected_farm, self)
        self.farm_manager.activate_farm(farm_name)

    def on_lb_farm_click(self, event):
        sender = event.GetEventObject()
        index = event.GetInt()
        farm_name = sender.GetString(index)

        self.activate_farm(farm_name)

    def activate_date(self, date):
        self.selected_date = date
        self.lb_old_dates.Deselect(self.lb_old_dates.GetSelection())
        self.lb_new_dates.Deselect(self.lb_new_dates.GetSelection())
        self.lb_old_dates.SetStringSelection(date)
        self.lb_new_dates.SetStringSelection(date)

    def on_lb_date_click(self, event):
        sender = event.GetEventObject()
        index = event.GetInt()
        date = sender.GetString(index)

        self.activate_date(date)

    def on_mark_done_click(self, _):
        if not self.selected_date:
            return

        self.farm_manager.change_date(self.selected_date, "old")
        self.activate_date(self.selected_date)

    def on_mark_not_done_click(self, _):
        if not self.selected_date:
            return

        self.farm_manager.change_date(self.selected_date, "new")
        self.activate_date(self.selected_date)

    def on_checkout_click(self, _):
        if not self.selected_date:
            error = "noDate"
        else:
            try:
                error = self.farm_manager.make_report(self.selected_date)
            except RequestConnectionError:
                dlg = OkDialog(
                    None,
                    "LKV Seite kann nicht erreicht werden (Internet aus?)",
                    title="Fehler",
                )
                dlg.ShowModal()
                dlg.Destroy()
                return

        text = 'Bericht fertig gestellt!\n\n("Berichte/%s/%s.pdf")' % (
            self.selected_farm,
            self.selected_date,
        )
        if error == "wrongPin":
            text = "PIN oder MLP Nummer falsch!"
        elif error == "noDate":
            text = "Kein Datum ausgewählt!"
        elif error == "hitAuthFailed":
            text = f"{self.selected_farm.name} konnte nicht gegen HIT authentifiziert werden."
        elif error == "wrongDate":
            text = "Datum für diesen Betrieb nicht verfügbar!"
        elif error == "fileExists":
            text = "Bericht für diese Datum ist bereits vorhanden. (Bitte den alten erst löschen!)"
        elif error == "nothingFound":
            text = "Nichts unter diesem Datum gefunden!"

        dlg = OkDialog(None, text, title="Bericht erstellen")
        dlg.ShowModal()
        dlg.Destroy()

    def on_borders_click(self, _):
        dlg = BorderDialog(None, title="Grenzen beabeiten")
        result = dlg.ShowModal()
        dlg.Destroy()

        if result != ID_OK or not dlg.borderMap:
            return

        with open("borders.txt", "w") as border_file:
            border_file.write(str(dlg.borderMap))

    def on_new_farm_click(self, _):
        error = self.farm_manager.add_farm()

        if error == "notComplete":
            dlg = OkDialog(
                None,
                "Name, MLP Nummer und PIN müssen ausgefüllt werden!",
                title="Neuer Betrieb",
            )
            dlg.ShowModal()
            dlg.Destroy()
        elif error == "nameExists":
            dlg = OkDialog(None, "Betrieb bereits vergeben!", title="Neuer Betrieb")
            dlg.ShowModal()
            dlg.Destroy()
        elif error == "dirExists":
            dlg = OkDialog(
                None,
                'Betrieb im Ordner "Berichte" bereits vorhanden. (Ordner erst löschen)',
                title="Neuer Betrieb",
            )
            dlg.ShowModal()
            dlg.Destroy()

    def on_change_farm_click(self, _):
        error = self.farm_manager.change_farm()

        if error == "noSelection":
            dlg = OkDialog(None, "Kein Betrieb ausgewählt!", title="Betrieb bearbeiten")
            dlg.ShowModal()
            dlg.Destroy()
        elif error == "notComplete":
            dlg = OkDialog(
                None,
                "Name, MLP Nummer und PIN müssen ausgefüllt werden!",
                title="Betrieb bearbeiten",
            )
            dlg.ShowModal()
            dlg.Destroy()
        elif error == "nameExists":
            dlg = OkDialog(None, "name für Betrieb bereits vergeben!", title="Betrieb bearbeiten")
            dlg.ShowModal()
            dlg.Destroy()
        elif error == "dirExists":
            dlg = OkDialog(
                None,
                'Name für Betrieb im Ordner "Berichte" bereits vorhanden. (Ordner erst löschen)',
                title="Betrieb bearbeiten",
            )
            dlg.ShowModal()
            dlg.Destroy()

        self.activate_farm(self.selected_farm)

    def on_del_farm_click(self, _):
        error = self.farm_manager.remove_farm()
        if error == "noSelection":
            dlg = OkDialog(None, "Kein Betrieb ausgewählt!", title="Betrieb löschen")
            dlg.ShowModal()
            dlg.Destroy()
        self.activate_farm(None)

    def __del__(self, *err):
        self.release()

    def __exit__(self, *err):
        self.release()
