import traceback
from wx import App
from window import Window

try:
    app = App()
    Window()
    app.MainLoop()
except:  # pylint: disable=bare-except
    ERROR_LOG = traceback.format_exc()
    print(ERROR_LOG)
    with open("log.txt", "w") as logfile:
        logfile.write(ERROR_LOG)
