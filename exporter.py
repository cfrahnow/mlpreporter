# -*- coding: utf-8 -*-

from shutil import rmtree as removeDir
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.platypus import (
    SimpleDocTemplate,
    Image,
    Table,
    Paragraph,
    Spacer,
    PageBreak,
    KeepTogether,
)
from reportlab.lib.units import mm
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

from diagram import Diagram


class Exporter:
    def __init__(self):
        self.danger_color = "#ffdddd"
        self.style_sheet = getSampleStyleSheet()
        self.style_sheet.add(ParagraphStyle(name="CenteredNormal", alignment=TA_CENTER))
        self.style_sheet.add(
            ParagraphStyle(
                name="Headline",
                alignment=TA_CENTER,
                parent=self.style_sheet["Heading2"],
                fontSize=18,
            )
        )
        self.diagram = Diagram()
        self.table_margin = (1 * mm, 1 * mm)
        self.translate = {
            u"SNR": u"SNR",
            u"Name": u"Name",
            u"Lebensnummer": u"Lebensnummer",
            u"Gruppe": u"Gruppe",
            u"Lakt. Zahl": u"Lakt. Zahl",
            u"Lakt. Tage": u"Lakt. Tage",
            u"ST": u"ST",
            u"Milch kg": u"Milch kg",
            u"Fett %": u"Fett %",
            u"Eiw %": u"Eiw %",
            u"ZZ": u"ZZ",
            u"FEQ": u"FEQ",
            u"Harnstoff": u"Harn -stoff",
            u"Harnstoff- Klasse": u"HKl",
        }
        self.document = []

    def get_cut_row(self, data, row_number, col_width, max_row_count):
        data_lines = 0
        for i_row, row in enumerate(data[1:]):
            # create fake paragraph for linebreak measures
            if not row[row_number]:
                data_lines += 1
            else:
                name_parag = Paragraph(row[row_number], self.style_sheet["CenteredNormal"])
                name_parag.width = col_width
                par_fragment = name_parag.breakLines(width=col_width)

                data_lines += len(par_fragment.lines)
            if data_lines > max_row_count:
                return i_row + 1

        return len(data)

    def make_header(self):
        # add logo to top
        image = Image("checkout/mr_logo.png")
        image.drawWidth = image.drawWidth * 3 / 4
        image.drawHeight = image.drawHeight * 3 / 4
        image.hAlign = "RIGHT"
        self.document.append(image)

    def make_summary_table1(self, data):
        ptext = "Milchmengen- und Milchinhaltstoffe (der MLPs der letzten Monate)"

        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]
        for row in data[1:]:
            row[0] = "<font size=8>%s</font>" % row[0]
        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        if data:
            table = Table(
                data,
                colWidths=[
                    19 * mm,
                    18 * mm,
                    22 * mm,
                    17 * mm,
                    14 * mm,
                    15 * mm,
                    16 * mm,
                    18 * mm,
                    14 * mm,
                    25 * mm,
                ],
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        self.document.append(Paragraph(ptext, self.style_sheet["Headline"]))
        self.document.append(Spacer(1, 10))
        if data:
            self.document.append(table)

    def make_summary_table2(self, data):
        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        if data:
            table = Table(
                data,
                colWidths=[40 * mm, 40 * mm, 40 * mm],
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )
            self.document.append(table)

    def make_milk_dist_diagram(self, data):
        if data:
            ptext = "Milchmengen-Verteilung"

            squared, dotted = data

            self.diagram.make_milk_dist_diag(squared, dotted, "milkDist.png")

            image = Image("temp/milkDist.png")
            image.drawWidth = image.drawWidth * 3 / 4
            image.drawHeight = image.drawHeight * 3 / 4
            image.hAlign = "CENTER"

            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"]), image]))

    def make_energy_fault(self, data):
        col_widths = [
            10 * mm,
            29 * mm,
            29 * mm,
            11 * mm,
            11 * mm,
            20 * mm,
            15 * mm,
            15 * mm,
            12 * mm,
            14 * mm,
        ]
        max_lines = 18

        ptext = "Deutlicher Energiemangel"

        cut_row = self.get_cut_row(
            data,
            1,
            col_widths[1] - (self.table_margin[0] + self.table_margin[1]),
            max_lines,
        )

        data = data[:cut_row]

        for i, row in enumerate(data[1:]):
            row[2] = "<font size=8>%s</font>" % row[2]

        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        if data:
            table = Table(
                data,
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-3, 1),
                        (-3, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        self.document.append(Paragraph(ptext, self.style_sheet["Headline"]))
        self.document.append(Spacer(1, 10))
        if data:
            self.document.append(table)

    def make_energy_supply_diagram(self, data):
        if data:
            ptext = "Energieversorgung"

            squared_data = list(map(list, zip(*[e[1] for e in data if e[0]])))
            dotted_data = list(map(list, zip(*[e[1] for e in data if not e[0]])))
            self.diagram.make_energy_supply_diag(squared_data, dotted_data, "energySupp.png")

            image = Image("temp/energySupp.png")
            image.drawWidth = image.drawWidth * 3 / 4
            image.drawHeight = image.drawHeight * 3 / 4
            image.hAlign = "CENTER"

            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"]), image]))

    def make_acidose_suspicion(self, data):
        col_widths = [
            10 * mm,
            29 * mm,
            29 * mm,
            11 * mm,
            11 * mm,
            20 * mm,
            15 * mm,
            15 * mm,
            12 * mm,
            14 * mm,
        ]
        max_lines = 18

        cut_row = self.get_cut_row(
            data,
            1,
            col_widths[1] - (self.table_margin[0] + self.table_margin[1]),
            max_lines,
        )

        data = data[:cut_row]

        ptext = "Acidose-Verdacht (Pansen sauer?)"
        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]
        for row in data[1:]:
            row[2] = "<font size=8>%s</font>" % row[2]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        if data:
            table = Table(
                data,
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-4, 1),
                        (-4, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        self.document.append(Paragraph(ptext, self.style_sheet["Headline"]))
        self.document.append(Spacer(1, 10))
        if data:
            self.document.append(table)

    def make_acidose_danger_diagram(self, data):
        if data:
            ptext = "Acidose-Gefahr (bzw. Körperfettabbau)"

            squared_data = list(map(list, zip(*[e[1] for e in data if e[0]])))
            dotted_data = list(map(list, zip(*[e[1] for e in data if not e[0]])))
            self.diagram.make_acidose_danger_diag(squared_data, dotted_data, "acidoseDanger.png")

            image = Image("temp/acidoseDanger.png")
            image.drawWidth = image.drawWidth * 3 / 4
            image.drawHeight = image.drawHeight * 3 / 4
            image.hAlign = "CENTER"

            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"]), image]))

    def make_ketose_danger(self, data):
        col_widths = [
            10 * mm,
            29 * mm,
            29 * mm,
            11 * mm,
            11 * mm,
            20 * mm,
            15 * mm,
            15 * mm,
            12 * mm,
            14 * mm,
        ]

        ptext = "Ketose-Gefahr"

        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]
        for row in data[1:]:
            row[2] = "<font size=8>%s</font>" % row[2]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        if data:
            table = Table(
                data,
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-4, 1),
                        (-4, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        self.document.append(Paragraph(ptext, self.style_sheet["Headline"]))
        self.document.append(Spacer(1, 10))
        if data:
            self.document.append(table)

    def make_cell_problems(self, data):
        col_widths = [
            10 * mm,
            29 * mm,
            29 * mm,
            11 * mm,
            11 * mm,
            20 * mm,
            15 * mm,
            15 * mm,
            12 * mm,
            14 * mm,
        ]
        ptext = "Zellen / Euterprobleme"

        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]
        for row in data[1:]:
            row[2] = "<font size=8>%s</font>" % row[2]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])

        # two tables, so that the first one always sticks to the headline to prevent the healine
        # beeing on a seperate page
        if data:
            table1 = Table(
                data[:5],
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-2, 1),
                        (-2, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )
        if len(data) > 5:
            table2 = Table(
                data[5:],
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-2, 0),
                        (-2, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        if data:
            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"]), Spacer(1, 10), table1]))
        else:
            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"])]))

        if len(data) > 5:
            self.document.append(table2)

    def make_special_cows(self, data):
        col_widths = [
            10 * mm,
            29 * mm,
            29 * mm,
            11 * mm,
            11 * mm,
            20 * mm,
            15 * mm,
            15 * mm,
            12 * mm,
            14 * mm,
        ]
        ptext = u"Auffallende Kühe (v.a. Milchverlust)"

        for i in range(len(data[0])):
            data[0][i] = "<b>%s</b>" % data[0][i]
        for row in data[1:]:
            row[2] = "<font size=8>%s</font>" % row[2]

        for row in data:
            for index, entry in enumerate(row):
                if isinstance(entry, str):
                    row[index] = Paragraph(entry, self.style_sheet["CenteredNormal"])
        if data:
            table1 = Table(
                data[:5],
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, 0), 1.0, colors.black),
                    ("GRID", (0, 1), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-4, 1),
                        (-4, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )
        if len(data) > 5:
            table2 = Table(
                data[5:],
                colWidths=col_widths,
                style=[
                    ("GRID", (0, 0), (-1, -1), 0.5, colors.black),
                    ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                    (
                        "BACKGROUND",
                        (-4, 0),
                        (-4, -1),
                        colors.HexColor(self.danger_color),
                    ),
                    ("LEFTPADDING", (0, 0), (-1, -1), self.table_margin[0]),
                    ("RIGHTPADDING", (0, 0), (-1, -1), self.table_margin[1]),
                ],
            )

        if data:
            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"]), Spacer(1, 10), table1]))
        else:
            self.document.append(KeepTogether([Paragraph(ptext, self.style_sheet["Headline"])]))
        if len(data) > 5:
            self.document.append(table2)

    def make_report(self, report_data, filename):
        doc_builder = SimpleDocTemplate(
            filename,
            pagesize=A4,
            rightMargin=48,
            leftMargin=48,
            topMargin=30,
            bottomMargin=18,
        )
        self.document = []

        ###########################################################

        self.make_header()
        self.document.append(Spacer(1, 20))
        self.make_summary_table1(report_data["summaryTable1"])
        self.document.append(Spacer(1, 45))
        self.make_summary_table2(report_data["summaryTable2"])
        self.document.append(Spacer(1, 20))
        self.make_milk_dist_diagram(report_data["milkDist"])
        self.document.append(PageBreak())

        ###########################################################

        self.make_energy_fault(report_data["energyFault"])
        self.document.append(Spacer(1, 20))
        self.make_energy_supply_diagram(report_data["energySupply"])
        self.document.append(PageBreak())

        ###########################################################

        self.make_acidose_suspicion(report_data["acidoseSuspicion"])
        self.document.append(Spacer(1, 20))
        self.make_acidose_danger_diagram(report_data["acidoseDanger"])
        self.document.append(PageBreak())

        ###########################################################

        self.make_ketose_danger(report_data["ketoseDanger"])
        self.document.append(Spacer(1, 20))
        self.make_cell_problems(report_data["cellProblems"])
        if "specialCows" in report_data:
            self.document.append(Spacer(1, 20))
            self.make_special_cows(report_data["specialCows"])

        ###########################################################

        doc_builder.build(self.document)
        removeDir("temp", ignore_errors=True)
