import subprocess

REQUIREMENTS = [
    ["requests", "", "requests"],
    ["matplotlib", "", "matplotlib"],
    ["bs4", "", "beautifulsoup4"],
    ["reportlab", "", "reportlab"],
    ["pyinstaller", "", "pyinstaller"],
    ["wx", "-U", "wxPython"],
]


def upgrade_pip():
    subprocess.run(["python", "-m", "pip", "install", "--upgrade", "pip"], check=True)


def install(package):
    _, args, install_name = package

    if args != "":
        subprocess.run(["pip", "install", args, install_name], check=True)
    else:
        subprocess.run(["pip", "install", install_name], check=True)


if __name__ == "__main__":
    upgrade_pip()
    for req in REQUIREMENTS:
        install(req)
