# -*- coding: utf-8 -*-

from decimal import Decimal, InvalidOperation, getcontext
from enum import Enum

from exporter import Exporter


class DataType(Enum):
    INT = 0
    FLOAT = 1
    STR = 2


def date_cmp(date):
    return int(date[6:] + date[3:5] + date[:2])


def number_cmp(num):
    if not num:
        return 0.0
    return num


def str_to_int(text):
    try:
        return int(text.replace(".", ""))
    except (ValueError, AttributeError):
        return ""


def str_to_float(text):
    try:
        return Decimal(text.replace(".", "").replace(",", "."))
    except (ValueError, InvalidOperation, AttributeError):
        return ""


class Checkout:
    def __init__(self, report_data, filename):
        self.report_data_in = report_data
        self.report_data_out = {"date": report_data["date"]}
        self.filename = filename
        self.exporter = Exporter()
        self.type_map_probe = {
            u"SNR": DataType.INT,
            u"Name": DataType.STR,
            u"Lebensnummer": DataType.STR,
            u"Rasse": DataType.STR,
            u"Tiergruppe": DataType.STR,
            u"Lakt. Zahl": DataType.INT,
            u"Lakt. Tage": DataType.INT,
            u"ST": DataType.STR,
            u"Milch kg": DataType.FLOAT,
            u"Fett %": DataType.FLOAT,
            u"Eiw %": DataType.FLOAT,
            u"ZZ": DataType.INT,
            u"FEQ": DataType.FLOAT,
            u"Harnstoff": DataType.INT,
            u"Harnstoff- Klasse": DataType.INT,
            u"Laktose": DataType.FLOAT,
        }
        self.type_map_summary = {
            u"Probedatum": DataType.STR,
            u"Kuhanzahl Gesamt": DataType.INT,
            u"Kuhanzahl Milch": DataType.INT,
            u"Durchschn. Lakttage": DataType.INT,
            u"Milch kg": DataType.FLOAT,
            u"Fett %": DataType.FLOAT,
            u"Eiwei\xdf %": DataType.FLOAT,
            u"Zellzahl": DataType.INT,
            u"FEQ": DataType.FLOAT,
            u"Harnstoff": DataType.FLOAT,
            u"Milch Kg Tag": DataType.FLOAT,
            u"Fett Kg Tag": DataType.FLOAT,
            u"Eiw. Kg Tag": DataType.FLOAT,
            u"Melkzeit M": DataType.STR,
            u"Melkzeit A": DataType.STR,
            u"ButtonName": DataType.STR,
            u"Laktose": DataType.FLOAT,
            u"Ges. ECM": DataType.FLOAT,
            u"Ges. Milch kg": DataType.FLOAT,
            u"Ges. Fett kg": DataType.FLOAT,
            u"Ges. Eiw. kg": DataType.FLOAT,
            u"A/M": DataType.STR,
            u"Art": DataType.STR,
        }
        self.convert_types()

    def convert_types(self):
        for data_name, type_map in [
            ("probes", self.type_map_probe),
            ("lastProbes", self.type_map_probe),
            ("summary", self.type_map_summary),
        ]:
            data = self.report_data_in[data_name]
            if not data:
                continue
            for col in range(len(data[0])):
                col_id = data[0][col]
                fct = lambda x: x
                if type_map[col_id] == DataType.INT:
                    fct = str_to_int
                elif type_map[col_id] == DataType.FLOAT:
                    fct = str_to_float
                for row in data[1]:
                    row[col] = fct(row[col])

    @staticmethod
    def sort_data_by(data, col_id, from_high, cmp_func):
        if not data:
            return
        try:
            col_index = data[0].index(col_id)
        except ValueError:
            return

        factor = 1
        if from_high:
            factor = -1

        data[1].sort(key=lambda x: cmp_func(x[col_index]) * factor)

    def make_summary_table1(self):
        self.sort_data_by(self.report_data_in["summary"], u"Probedatum", False, date_cmp)
        head, dates = self.report_data_in["summary"]
        summary_out_header = [
            u"Probedatum",
            u"Kuhanzahl Gesamt",
            u"Kuhanzahl Milch",
            u"Durchschn. Lakttage",
            u"Milch kg",
            u"Fett %",
            u"Eiwei\xdf %",
            u"Zellzahl",
            u"Harnstoff",
            u"Ges. Milch kg",
        ]

        self.report_data_out["summaryTable1"] = [
            [
                u"Datum",
                u"Kuhzahl\ngesamt",
                u"Kuhzahl\ngemolken",
                u"\u2205 Lakt.\nTage",
                u"Milch\nkg",
                u"Fett %",
                u"Eiweiß %",
                u"Zell-Zahl",
                u"Harn-\nstoff",
                u"Milch-Menge",
            ]
        ]
        for i in range(len(dates)):
            self.report_data_out["summaryTable1"].append([])

        for col_name in summary_out_header:
            col_index = head.index(col_name)
            for i, row in enumerate(dates):
                self.report_data_out["summaryTable1"][i + 1].append(row[col_index])

    def make_summary_table2(self):
        head, probes = self.report_data_in["probes"]
        summary_out_header = [u"Laktationstage", u"Kuhanzahl", u"\u2205 Milchmenge in kg"]

        counts = [0, 0, 0]
        milk_values = [Decimal(0), Decimal(0), Decimal(0)]

        lak_index = head.index("Lakt. Tage")
        milk_index = head.index("Milch kg")
        for row in probes:
            laktation = row[lak_index]
            milk = row[milk_index]
            if milk == "" or laktation == "":
                continue
            index = -1
            if 0 < laktation < 101:
                index = 0
            elif laktation < 201:
                index = 1
            elif laktation >= 201:
                index = 2
            else:
                continue

            counts[index] += 1
            milk_values[index] += milk
        for i in range(3):
            if counts[i] > 0:
                milk_values[i] /= counts[i]

        getcontext().prec = 3
        self.report_data_out["summaryTable2"] = [summary_out_header]
        self.report_data_out["summaryTable2"].append([u"1-100", counts[0], +milk_values[0]])
        self.report_data_out["summaryTable2"].append([u"101-200", counts[1], +milk_values[1]])
        self.report_data_out["summaryTable2"].append([u"über 200", counts[2], +milk_values[2]])
        getcontext().prec = 28

    def make_milk_distribution_diagram(self):
        head, data = self.report_data_in["probes"]
        lak_num_index = head.index("Lakt. Zahl")
        lak_days_index = head.index("Lakt. Tage")
        milk_index = head.index("Milch kg")
        lak_days_data = []
        milk_data = []
        lak_days_data_first_laktation = []
        milk_data_first_laktation = []

        for row in data:
            lak_num = row[lak_num_index]
            lak_days = row[lak_days_index]
            milk = row[milk_index]
            if lak_days == "" or milk == "" or lak_num == "":
                continue
            if lak_num <= 1:
                lak_days_data_first_laktation.append(lak_days)
                milk_data_first_laktation.append(milk)
            else:
                lak_days_data.append(lak_days)
                milk_data.append(milk)

        self.report_data_out["milkDist"] = (
            (lak_days_data_first_laktation, milk_data_first_laktation),
            (lak_days_data, milk_data),
        )

    def make_energy_fault(self):
        self.sort_data_by(self.report_data_in["probes"], u"Eiw %", False, number_cmp)
        head, data = self.report_data_in["probes"]

        protein_index = head.index(u"Eiw %")
        lak_days_index = head.index("Lakt. Tage")
        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Fett %",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["energyFault"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Fett %",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if (
                row[protein_index] == ""
                or row[lak_days_index] == ""
                or row[protein_index] > 3.2
                or row[lak_days_index] > 160
            ):
                continue
            self.report_data_out["energyFault"].append([row[index] for index in out_indexes])

    def make_energy_supply_diagram(self):
        diag_data = []
        head, data = self.report_data_in["probes"]

        protein_index = head.index(u"Eiw %")
        milk_index = head.index("Milch kg")
        lak_num_index = head.index("Lakt. Zahl")

        for row in data:
            lak_num = row[lak_num_index]
            milk = row[milk_index]
            protein = row[protein_index]
            if lak_num == "" or milk == "" or protein == "":
                continue
            diag_data.append([False if lak_num > 1 else True, [milk, protein]])

        self.report_data_out["energySupply"] = diag_data

    def make_acidose_suspicion(self):
        self.sort_data_by(self.report_data_in["probes"], u"Fett %", False, number_cmp)
        head, data = self.report_data_in["probes"]

        fat_index = head.index(u"Fett %")
        lak_days_index = head.index("Lakt. Tage")
        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Fett %",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["acidoseSuspicion"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Fett %",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if row[fat_index] == "" or row[lak_days_index] == "" or row[fat_index] > 3.7 or row[lak_days_index] > 160:
                continue
            self.report_data_out["acidoseSuspicion"].append([row[index] for index in out_indexes])

    def make_acidose_danger_diagram(self):
        diag_data = []
        head, data = self.report_data_in["probes"]

        fat_index = head.index(u"Fett %")
        milk_index = head.index("Milch kg")
        lak_num_index = head.index("Lakt. Zahl")

        for row in data:
            lak_num = row[lak_num_index]
            milk = row[milk_index]
            fat = row[fat_index]
            if lak_num == "" or milk == "" or fat == "":
                continue
            diag_data.append([False if lak_num > 1 else True, [milk, fat]])

        self.report_data_out["acidoseDanger"] = diag_data

    def make_ketose_danger(self):
        self.sort_data_by(self.report_data_in["probes"], u"Fett %", True, number_cmp)
        head, data = self.report_data_in["probes"]

        fat_index = head.index(u"Fett %")
        lak_days_index = head.index("Lakt. Tage")
        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Fett %",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["ketoseDanger"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Fett %",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if row[fat_index] == "" or row[lak_days_index] == "" or row[fat_index] < 4.0 or row[lak_days_index] > 50:
                continue
            self.report_data_out["ketoseDanger"].append([row[index] for index in out_indexes])

    def make_cell_problems(self):
        self.sort_data_by(self.report_data_in["probes"], u"ZZ", True, number_cmp)
        head, data = self.report_data_in["probes"]

        zz_index = head.index(u"ZZ")
        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Fett %",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["cellProblems"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Fett %",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if row[zz_index] == "" or row[zz_index] < 250:
                continue
            self.report_data_out["cellProblems"].append([row[index] for index in out_indexes])

    def make_special_cows(self):
        head, data = self.report_data_in["probes"]
        last_head, last_data = self.report_data_in["lastProbes"]

        if not last_head or not last_data:
            return
        lnr_index = head.index(u"Lebensnummer")
        milk_index = head.index("Milch kg")
        last_lnr_index = last_head.index(u"Lebensnummer")
        last_milk_index = last_head.index("Milch kg")

        milk_values = {}
        for row in data:
            if row[milk_index] == "" or row[lnr_index] == "":
                continue
            milk_values[row[lnr_index]] = row[milk_index]

        milk_diff_values = {}
        for row in last_data:
            lnr = row[last_lnr_index]
            if lnr == "" or row[last_milk_index] == "":
                continue
            if lnr in milk_values:
                milk_diff_values[lnr] = milk_values[lnr] - row[last_milk_index]

        head.append(u"Milch kg Diff")
        for row in data:
            lnr = row[lnr_index]
            if lnr in milk_diff_values:
                row.append(milk_diff_values[lnr])
            else:
                row.append("")

        self.sort_data_by(self.report_data_in["probes"], u"Milch kg Diff", False, number_cmp)

        lak_days_index = head.index("Lakt. Tage")
        milk_index = head.index(u"Milch kg")
        diff_milk_index = head.index(u"Milch kg Diff")

        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Milch kg Diff",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["specialCows"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Milch kg Diff.",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if row[diff_milk_index] == "" or row[milk_index] == "":
                diff_percent = 1
            else:
                diff_percent = row[diff_milk_index] / row[milk_index]

            if (
                row[lak_days_index] == ""
                or row[milk_index] == ""
                or row[diff_milk_index] == ""
                or row[lak_days_index] > 240
                or diff_percent > -0.12
            ):
                continue
            self.report_data_out["specialCows"].append([row[index] for index in out_indexes])

    def make_udder_health(self):
        self.sort_data_by(self.report_data_in["probes"], u"ZZ", True, number_cmp)
        head, data = self.report_data_in["probes"]

        zz_index = head.index(u"ZZ")
        out_header = [
            u"SNR",
            u"Name",
            u"Lebensnummer",
            u"Lakt. Zahl",
            u"Lakt. Tage",
            u"Milch kg",
            u"Fett %",
            u"Eiw %",
            u"ZZ",
            u"FEQ",
        ]
        out_indexes = [head.index(s) for s in out_header]

        self.report_data_out["udderHealth"] = [
            [
                u"Nr.",
                u"Name",
                u"Lebensnummer",
                u"Lakt. Zahl",
                u"Lakt. Tage",
                u"Milch kg",
                u"Fett %",
                u"Eiw %",
                u"ZZ",
                u"FEQ",
            ]
        ]

        for row in data:
            if row[zz_index] == "" or row[zz_index] < 250:
                continue
            self.report_data_out["udderHealth"].append([row[index] for index in out_indexes])

    def generate_complete_dataset(self):
        self.make_summary_table1()
        self.make_summary_table2()
        self.make_milk_distribution_diagram()
        self.make_energy_fault()
        self.make_energy_supply_diagram()
        self.make_acidose_suspicion()
        self.make_acidose_danger_diagram()
        self.make_ketose_danger()
        self.make_cell_problems()
        self.make_special_cows()
        self.make_udder_health()

    def generate_report(self):
        self.generate_complete_dataset()
        self.exporter.make_report(self.report_data_out, self.filename)
